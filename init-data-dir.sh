#!/bin/bash

IMAGE_NAME="insecurity/nginx:10.1"
DATADIR="./Data"

if [ -d "$DATADIR" ]
then
	echo -n "WARNING: The directory $DATADIR is not empty, all configuration and log files will be delete. Do you want to re-initialise $DATADIR folder ? [o/n]: "
	read DELETE
	if [ "$DELETE" == "o" ];
	then
        	echo "Try to down existing container"
		docker-compose down
		echo "Remove $DATADIR directory"
		rm -Rdv $DATADIR
	fi
	./init-data-dir.sh
else
	echo "Creation of directory $DATADIR"
        mkdir -p $DATADIR/etc/
        mkdir -p $DATADIR/log/nginx/
        touch $DATADIR/log/nginx/error.log
        #chmod 766 $DATADIR/log/nginx/error.log
	echo "Run tmp container"
        docker run --user "nginx:nginx" --name INIT -d $IMAGE_NAME

	echo "Init $DATADIR directory with container files"
        docker cp INIT:/etc/nginx/ $DATADIR/etc/nginx/
        docker cp INIT:/usr/share/nginx/ $DATADIR/www/
        docker rm -f INIT

        echo "Chown 1000:1000 (nginx) $DATADIR"
        chown -R 1000:1000 $DATADIR
        chmod -R 777 $DATADIR

        echo "$DATADIR initialized"
	echo "Create and run final container with docker-compose.yml file"
        docker-compose up -d
	docker ps -a
fi
