## Nginx Docker image ##
Autobuilded `nginx` image base on up-to-date `debian:jessie` (x64 architecture).

### Git branch & Docker tags ###
- `latest` => `1.14.0-1` / branch master 
- `1.14.0-1` / branch 1.14.0
- `1.12.2-1` / branch 1.12.2
- `1.10.3-1` / branch 1.10.3
- `1.10.1-1` / branch 1.10.1 

### Image build & optimization ###

This image is "one layer image", all `RUN` commands was group into one in order to reduce the final image size. 
Moreover, every `ADD` or `COPY` commands point on an external URL instead of local files. So if you want to rebuild the image, it's not necessary to pull any git repository before, you could just do:

    docker build -t "nginx:1.14.0-1" https://bitbucket.org/jtof_fap/secure-nginx/raw/master/Dockerfile

Nginx version is an argument (ARG) in Dockerfile, so you could change nginx version on the fly in build process.

    docker build -t "nginx:1.14.0-1" --build-arg VERSION=1.14.0-1 https://bitbucket.org/jtof_fap/secure-nginx/raw/master/Dockerfile

Version numbers are available here: [http://nginx.org/packages/debian/pool/nginx/n/nginx/](http://nginx.org/packages/debian/pool/nginx/n/nginx/)

### Image automatic updates ###

This image is automatically rebuilt when: 

- When base image is updated: 

This image is based on an up-to-date debian image (`insecurity/debian:jessie`) and is automatically rebuild when base image is updated. For more information about this process, reports to: 
[https://hub.docker.com/r/insecurity/debian/](https://hub.docker.com/r/insecurity/debian/) 

- When git repository is updated ([https://bitbucket.org/jtof_fap/secure-nginx/](https://bitbucket.org/jtof_fap/secure-nginx/)):

### Getting this image ###

    docker pull insecurity/secure-nginx:latest
    docker pull insecurity/secure-nginx:1.14.0-1
    docker pull insecurity/secure-nginx:1.12.2-1
    docker pull insecurity/secure-nginx:1.10.3-1
    docker pull insecurity/secure-nginx:1.10.1-1

### Quick run this image ###

Using Git:

    git clone https://jtof_fap@bitbucket.org/jtof_fap/secure-nginx.git && cd secure-nginx
    mkdir -p ./Data/etc/nginx/ ./Data/log/nginx/ ./Data/www/ && touch ./Data/log/nginx/error.log && chown -R 3005:3005 ./Data && chmod -R 700 ./Data
    cp Compose/docker-compose.yml ./
    docker-compose up -d

Without using Git:

    mkdir Nginx && cd Nginx
    mkdir -p ./Data/etc/nginx/ ./Data/log/nginx/ ./Data/www/ && touch ./Data/log/nginx/error.log && chown -R 3005:3005 ./Data && chmod -R 700 ./Data
    wget https://bitbucket.org/jtof_fap/secure-nginx/raw/master/Dockerfile
    wget https://bitbucket.org/jtof_fap/secure-nginx/raw/master/Compose/docker-compose.yml
    docker-compose up -d
    
### Persistence configuration ###

To run this image, you could use the following `docker-compose.yml` example file: 

- [https://bitbucket.org/jtof_fap/secure-nginx/raw/master/Compose/docker-compose.yml](https://bitbucket.org/jtof_fap/secure-nginx/raw/master/Compose/docker-compose.yml) 

This container use 3 volumes :

    $DATADIR/etc/nginx/:/etc/nginx/ => CONFIGURATION
    $DATADIR/www/:/usr/share/nginx/ => WWW
    $DATADIR/log/:/var/log/nginx/   => LOGS

### Container security ###

In order to limit the security impacts in case of nginx compromission, this container is a `non-root-user-container`.

- **Container runs with `limited` user**

To do this, some changes are required from other "classic" `nginx` images. **Non-root** user **can't bind ports < 1024** so default port (EXPOSE) are change to `8080` and `8443`. The **Dockerfile** affects also 3005:3005 `UID/GID` to nginx user and `run.sh` change all permissions for this user in local volumes.

In configuration, PID is set to `/etc/nginx/nginx.pid` instead of `/var/run` and the listening port is change to **8080** in default configuration file.

- **Capabilities are limited in docker-compose file**:

Moreover, all capabilities allowed by default on docker container could be dropped (with docker-compose):

    cap_drop:
      - ALL



